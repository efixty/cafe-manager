-- users
create table users
(
  id       bigserial    not null
    constraint user_pkey
      primary key,
  username varchar(40)  not null
    unique,
  password varchar(255) not null,
  name     varchar(255) not null,
  role     integer      not null
);

insert into users                                      -- awesome                                                            manager
values (nextval('users_id_seq'), 'azaryan_maria_art', '$2a$09$rei0LaOFKZfKaV7Qo3Z2Ze4or5uVQzj41AAmBUZE3lO65y6ycTyMa', 'Maria', 0);
insert into users                                      -- ecolden                                                  waiter
values (nextval('users_id_seq'), 'efixty', '$2a$09$dnn9eo5GTHS7.TE/w8gqMu37uzbmp2A34op3FNDJ//BwjOc5Hu9Bm', 'Arthur', 1);

-- tables
create table tables
(
  id         bigserial not null
    constraint tables_pkey
      primary key,
  assigned   boolean   not null,
  seat_count smallint  not null,
  status     integer   not null,
  user_id    bigint
    constraint fkld8mjwpsleu1g0lcd7jm4nwde
      references users
);

insert into tables
values (nextval('tables_id_seq'), false, 4, 0, null);
insert into tables
values (nextval('tables_id_seq'), false, 4, 0, null);
insert into tables
values (nextval('tables_id_seq'), false, 2, 0, null);
insert into tables
values (nextval('tables_id_seq'), false, 8, 0, null);

-- orders
create table orders
(
  id       bigserial not null
    constraint orders_pkey
      primary key,
  status   integer   not null,
  table_id bigint    not null
    constraint fkrkhrp1dape261t3x3spj7l5ny
      references tables
);

-- product
create table product
(
  id    bigserial        not null
    constraint product_pkey
      primary key,
  name  varchar(255)     not null,
  price double precision not null
);

insert into product
values (nextval('product_id_seq'), 'Banana', 3.0);
insert into product
values (nextval('product_id_seq'), 'Kiwi', 2.0);

-- product_in_order
create table product_in_order
(
  id         bigserial not null
    constraint product_in_order_pkey
      primary key,
  status     integer   not null,
  quantity   integer   not null,
  order_id   bigint    not null
    constraint fkl37w7v97lwh833c4gc2qotnxt
      references orders,
  product_id bigint    not null
    constraint fk7qpekps6q9cyrhax6hfu3m2gm
      references product
);