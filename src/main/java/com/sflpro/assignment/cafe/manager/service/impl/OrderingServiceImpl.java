package com.sflpro.assignment.cafe.manager.service.impl;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.order.OrderCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.order.OrderPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserPrincipal;
import com.sflpro.assignment.cafe.manager.domain.entity.OrderEntity;
import com.sflpro.assignment.cafe.manager.domain.entity.ProductInOrderEntity;
import com.sflpro.assignment.cafe.manager.domain.entity.TableEntity;
import com.sflpro.assignment.cafe.manager.domain.enums.OrderStatus;
import com.sflpro.assignment.cafe.manager.domain.enums.OrderedProductStatus;
import com.sflpro.assignment.cafe.manager.domain.enums.TableStatus;
import com.sflpro.assignment.cafe.manager.domain.exception.NotFoundException;
import com.sflpro.assignment.cafe.manager.domain.mapper.OrderMapper;
import com.sflpro.assignment.cafe.manager.domain.mapper.ProductInOrderMapper;
import com.sflpro.assignment.cafe.manager.repository.OrderRepository;
import com.sflpro.assignment.cafe.manager.service.OrderingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderingServiceImpl implements OrderingService {

    private final ResourceBundle bundle;

    private final TableServiceImpl tableService;
    private final ProductServiceImpl productService;

    private final OrderRepository orderRepository;

    private final OrderMapper orderMapper;
    private final ProductInOrderMapper productInOrderMapper;

    public OrderingServiceImpl(ResourceBundle bundle, TableServiceImpl tableService,
                               ProductServiceImpl productService,
                               OrderRepository orderRepository, OrderMapper orderMapper,
                               ProductInOrderMapper productInOrderMapper) {
        this.bundle = bundle;
        this.tableService = tableService;
        this.productService = productService;
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.productInOrderMapper = productInOrderMapper;
    }

    @Transactional
    @Override
    public OrderPayload createOrder(OrderCreationPayload orderCreationPayload) {
        TableEntity tableEntity = tableService.getTableById(orderCreationPayload.getTableId());
        tableService.validateTableAvailability(tableEntity);
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderStatus(OrderStatus.ACTIVE);
        tableEntity.setTableStatus(TableStatus.USED);
        orderEntity.setTableEntity(tableEntity);
        OrderEntity createdOrder = orderRepository.save(orderEntity);
        List<ProductInOrderEntity> productsOrdered = orderCreationPayload.getProductInOrderCreationPayloads()
                .stream()
                .map(productInOrderCreationPayload -> populateEntityFromCreationPayload(createdOrder, productInOrderCreationPayload))
                .collect(Collectors.toList());
        createdOrder.setProductsInOrder(productsOrdered);
        OrderEntity savedEntity = orderRepository.save(orderEntity);
        return orderMapper.map(savedEntity);
    }

    @Transactional
    @Override
    public void cancelOrder(Long tableId) {
        OrderEntity orderEntity = orderRepository.getActiveOrderByTableId(tableId);
        validateOrderPresence(orderEntity);
        orderEntity.getTableEntity().setTableStatus(TableStatus.EMPTY);
        orderEntity.setOrderStatus(OrderStatus.CANCELED);
        orderRepository.save(orderEntity);
    }

    @Transactional
    @Override
    public Double closeOrder(Long tableId) {
        OrderEntity orderEntity = orderRepository.getActiveOrderByTableId(tableId);
        validateOrderPresence(orderEntity);
        orderEntity.getTableEntity().setTableStatus(TableStatus.EMPTY);
        orderEntity.setOrderStatus(OrderStatus.CLOSED);
        List<ProductInOrderEntity> productsInOrder = orderRepository.save(orderEntity).getProductsInOrder();
        return calculateTotal(productsInOrder);
    }

    @Override
    public List<ProductInOrderPayload> updateProductsInOrder(Long tableId, List<ProductInOrderCreationPayload> newProductsInOrder) {
        OrderEntity orderToUpdate = orderRepository.getActiveOrderByTableId(tableId);
        validateOrderPresence(orderToUpdate);
        List<ProductInOrderEntity> newProductInOrderEntityList = newProductsInOrder.stream()
                .map(orderEntity -> populateEntityFromCreationPayload(orderToUpdate, orderEntity))
                .collect(Collectors.toList());
        orderToUpdate.setProductsInOrder(updateOrder(orderToUpdate.getProductsInOrder(), newProductInOrderEntityList));
        return orderRepository.save(orderToUpdate).getProductsInOrder()
                .stream()
                .map(productInOrderMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<OrderPayload> getActiveOrders() {
        Long waiterId = ((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        return orderRepository.getActiveOrdersByWaiterId(waiterId)
                .stream()
                .map(orderMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public OrderEntity getOrderById(Long orderId) {
        return orderRepository.findById(orderId).orElseThrow(() -> {
            final String errorMsg = bundle.getString("cafe.manager.order.not.found");
            log.error(errorMsg);
            return new NotFoundException(errorMsg);
        });
    }

    /**
     * Calculates total amount to be payed based on local cache of products.
     * If product's status is not active, then it is ignored
     *
     * @param productsInOrder list of products in order to form total
     * @return total amount to be payed
     */
    private double calculateTotal(List<ProductInOrderEntity> productsInOrder) {
        return productsInOrder.stream()
                .filter(orderedProduct -> orderedProduct.getOrderedProductStatus() == OrderedProductStatus.ACTIVE)
                .mapToDouble(productInOrderEntity -> productService.getMenuTreeMap().get(productInOrderEntity.getProduct().getId()).getPrice() * productInOrderEntity.getQuantity())
                .sum();
    }

    /**
     * Maps everything of ProductInOrderCreationPayload to ProductInOrderEntity.
     *
     * @param orderEntity                   order to which productInOrderCreationPayloads belong
     * @param productInOrderCreationPayload products of the given order
     * @return populated entity
     */
    private ProductInOrderEntity populateEntityFromCreationPayload(OrderEntity orderEntity, ProductInOrderCreationPayload productInOrderCreationPayload) {
        ProductInOrderEntity productInOrderEntity = new ProductInOrderEntity();
        productInOrderEntity.setOrderedProductStatus(productInOrderCreationPayload.getOrderedProductStatus());
        productInOrderEntity.setOrder(orderEntity);
        productInOrderEntity.setProduct(productService.getMenuTreeMap().get(productInOrderCreationPayload.getProductId()));
        productInOrderEntity.setQuantity(productInOrderCreationPayload.getQuantity());
        return productInOrderEntity;
    }

    /**
     * Updates current products in order list with new products in order list.
     * Uses TreeMap for time reasons, because the second list is almost always way
     * too smaller than first.
     *
     * @param productsInOrder             products that are currently contained by the order
     * @param newProductInOrderEntityList products to update order with
     * @return list of updated products
     */
    private List<ProductInOrderEntity> updateOrder(List<ProductInOrderEntity> productsInOrder, List<ProductInOrderEntity> newProductInOrderEntityList) {
        TreeMap<Long, ProductInOrderEntity> productsInOrderTreeMap = productsInOrder.stream().collect(Collectors.toMap(
                productInOrderEntity -> productInOrderEntity.getProduct().getId(),
                productInOrderEntity -> productInOrderEntity,
                (value1, value2) -> value2,
                TreeMap::new
        ));
        List<ProductInOrderEntity> newProductsInOrder =
                newProductInOrderEntityList
                        .stream()
                        .peek(productInOrderEntity -> productsInOrderTreeMap.remove(productInOrderEntity.getProduct().getId()))
                        .collect(Collectors.toList());

        newProductsInOrder.addAll(productsInOrderTreeMap.values());
        return newProductsInOrder;
    }

    /**
     * Checks if order exist
     *
     * @param orderEntity order to be checked
     * @throws NotFoundException if the order is not present
     */
    private void validateOrderPresence(OrderEntity orderEntity) {
        if (orderEntity == null) {
            final String errorMsg = bundle.getString("cafe.manager.order.not.found");
            log.error(errorMsg);
            throw new NotFoundException(errorMsg);
        }
    }
}