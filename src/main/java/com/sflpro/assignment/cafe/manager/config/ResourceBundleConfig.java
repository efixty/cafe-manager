package com.sflpro.assignment.cafe.manager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ResourceBundle;

@Configuration
public class ResourceBundleConfig {

    @Bean
    public ResourceBundle messagesResourceBundle() {
        return ResourceBundle.getBundle("messages");
    }
}
