package com.sflpro.assignment.cafe.manager.service.impl;

import com.sflpro.assignment.cafe.manager.domain.dto.table.TableCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.table.TablePayload;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserPrincipal;
import com.sflpro.assignment.cafe.manager.domain.entity.TableEntity;
import com.sflpro.assignment.cafe.manager.domain.enums.TableStatus;
import com.sflpro.assignment.cafe.manager.domain.exception.BadRequestException;
import com.sflpro.assignment.cafe.manager.domain.exception.NotFoundException;
import com.sflpro.assignment.cafe.manager.domain.mapper.TableMapper;
import com.sflpro.assignment.cafe.manager.repository.TableRepository;
import com.sflpro.assignment.cafe.manager.service.TableService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TableServiceImpl implements TableService {

    private final ResourceBundle bundle;

    private final TableRepository tableRepository;

    private final TableMapper tableMapper;

    public TableServiceImpl(ResourceBundle bundle,
                            TableRepository tableRepository, TableMapper tableMapper) {
        this.bundle = bundle;
        this.tableRepository = tableRepository;
        this.tableMapper = tableMapper;
    }

    @Override
    public void createTable(TableCreationPayload tableCreationPayload) {
        TableEntity tableEntity = new TableEntity();
        tableEntity.setTableStatus(TableStatus.EMPTY);
        tableEntity.setSeatCount(tableCreationPayload.getSeatCount());
        tableEntity.setAssigned(false);
        tableRepository.save(tableEntity);
    }

    @Override
    public List<TablePayload> getAllTables() {
        return tableRepository.findAll().stream().map(tableMapper::map).collect(Collectors.toList());
    }

    @Override
    public List<TablePayload> getTablePayloads() {
        Long waiterId = ((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        return tableRepository.findAllByWaiterId(waiterId).stream().map(tableMapper::map).collect(Collectors.toList());
    }

    @Override
    public void validateTableAvailability(TableEntity tableEntity) {
        if (!tableEntity.getAssigned()) {
            final String errorMsg = bundle.getString("cafe.manager.table.has.no.waiter");
            log.error(errorMsg);
            throw new BadRequestException(errorMsg);
        }
        if (tableEntity.getTableStatus() == TableStatus.USED) {
            final String errorMsg = bundle.getString("cafe.manager.table.not.empty");
            log.error(errorMsg);
            throw new BadRequestException(errorMsg);
        }
    }

    @Override
    public TableEntity getTableById(Long tableId) {
        return tableRepository.findById(tableId).orElseThrow(() -> {
            final String errorMsg = bundle.getString("cafe.manager.table.not.found");
            log.error(errorMsg);
            return new NotFoundException(errorMsg);
        });
    }
}
