package com.sflpro.assignment.cafe.manager.domain.mapper;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductPayload;
import com.sflpro.assignment.cafe.manager.domain.entity.ProductEntity;
import com.sflpro.assignment.cafe.manager.domain.mapper.base.Mapper;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper implements Mapper<ProductEntity, ProductPayload> {

    @Override
    public ProductPayload map(ProductEntity productEntity) {
        ProductPayload productPayload = new ProductPayload();
        productPayload.setId(productEntity.getId());
        productPayload.setName(productEntity.getName());
        productPayload.setPrice(productEntity.getPrice());
        return productPayload;
    }

    @Override
    public ProductEntity map(ProductPayload productPayload) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setId(productPayload.getId());
        productEntity.setName(productPayload.getName());
        productEntity.setPrice(productPayload.getPrice());
        return productEntity;
    }
}
