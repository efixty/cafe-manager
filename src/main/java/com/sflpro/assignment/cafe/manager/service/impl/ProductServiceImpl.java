package com.sflpro.assignment.cafe.manager.service.impl;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductPayload;
import com.sflpro.assignment.cafe.manager.domain.entity.ProductEntity;
import com.sflpro.assignment.cafe.manager.domain.entity.base.BaseEntity;
import com.sflpro.assignment.cafe.manager.domain.mapper.ProductMapper;
import com.sflpro.assignment.cafe.manager.repository.ProductRepository;
import com.sflpro.assignment.cafe.manager.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;
    private final Object menuLock = new Object();
    /**
     * This cache is intended for fast access to products by their IDs.
     * It should be refreshed every time new product is added to menu.
     * This field must be in synchronized context everywhere it is used.
     * If menu gets updated no one should use this field until it (cache) is synced with database.
     *
     * @see ProductServiceImpl#refreshCache()
     */
    private TreeMap<Long, ProductEntity> menu;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        refreshCache();
    }

    @Override
    public void createProduct(ProductCreationPayload productCreationPayload) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setName(productCreationPayload.getName());
        productEntity.setPrice(productCreationPayload.getPrice());
        productRepository.save(productEntity);
        refreshCache();
    }

    @Override
    public List<ProductPayload> getMenu() {
        synchronized (menuLock) {
            return this.menu.values().stream().map(productMapper::map).collect(Collectors.toList());
        }
    }

    TreeMap<Long, ProductEntity> getMenuTreeMap() {
        synchronized (menuLock) {
            return this.menu;
        }
    }

    private void refreshCache() {
        synchronized (menuLock) {
            this.menu = productRepository.findAll().stream().collect(Collectors.toMap(
                    BaseEntity::getId,
                    productEntity -> productEntity,
                    (value1, value2) -> value2,
                    TreeMap::new
            ));
        }
        log.info("Cache refreshed");
    }
}
