package com.sflpro.assignment.cafe.manager.domain.dto.table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TableAssignmentPayload {

    Long userId;
    Long tableId;

}
