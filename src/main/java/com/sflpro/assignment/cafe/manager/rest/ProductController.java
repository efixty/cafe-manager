package com.sflpro.assignment.cafe.manager.rest;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductCreationPayload;
import com.sflpro.assignment.cafe.manager.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    /**
     * Creates a new product from given payload.
     *
     * @param productCreationPayload dto, containing all necessary information for product creation
     * @return
     */
    @PreAuthorize("hasRole('MANAGER')")
    @PostMapping("/create")
    public ResponseEntity<?> createProduct(@RequestBody ProductCreationPayload productCreationPayload) {
        productService.createProduct(productCreationPayload);
        return ResponseEntity.ok().build();
    }

    /**
     * Fetches all products from local cache.
     *
     * @return list of products
     */
    @PreAuthorize("hasRole('WAITER')")
    @GetMapping("/menu")
    public ResponseEntity<?> getMenu() {
        return ResponseEntity.ok(productService.getMenu());
    }

}
