package com.sflpro.assignment.cafe.manager.domain.entity;

import com.sflpro.assignment.cafe.manager.domain.entity.base.BaseEntity;
import com.sflpro.assignment.cafe.manager.domain.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "USERS")
public class UserEntity extends BaseEntity {

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Role role;

    @OneToMany(mappedBy = "userEntity")
    private List<TableEntity> tables;

}
