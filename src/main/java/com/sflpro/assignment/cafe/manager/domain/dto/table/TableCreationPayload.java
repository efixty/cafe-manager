package com.sflpro.assignment.cafe.manager.domain.dto.table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TableCreationPayload {

    private byte seatCount;

}
