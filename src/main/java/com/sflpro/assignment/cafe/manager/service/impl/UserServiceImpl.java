package com.sflpro.assignment.cafe.manager.service.impl;

import com.sflpro.assignment.cafe.manager.domain.dto.table.TableAssignmentPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserPrincipal;
import com.sflpro.assignment.cafe.manager.domain.entity.TableEntity;
import com.sflpro.assignment.cafe.manager.domain.entity.UserEntity;
import com.sflpro.assignment.cafe.manager.domain.enums.Role;
import com.sflpro.assignment.cafe.manager.domain.enums.TableStatus;
import com.sflpro.assignment.cafe.manager.domain.exception.BadRequestException;
import com.sflpro.assignment.cafe.manager.domain.exception.NotFoundException;
import com.sflpro.assignment.cafe.manager.domain.mapper.UserMapper;
import com.sflpro.assignment.cafe.manager.repository.UserRepository;
import com.sflpro.assignment.cafe.manager.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private final ResourceBundle bundle;

    private final BCryptPasswordEncoder passwordEncoder;

    private final TableServiceImpl tableService;

    private final UserRepository userRepository;

    private final UserMapper userMapper;


    public UserServiceImpl(ResourceBundle bundle, BCryptPasswordEncoder passwordEncoder, TableServiceImpl tableService,
                           UserRepository userRepository, UserMapper userMapper) {
        this.bundle = bundle;
        this.passwordEncoder = passwordEncoder;
        this.tableService = tableService;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public void createUser(UserCreationPayload userCreationPayload) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(userCreationPayload.getUsername());
        userEntity.setPassword(passwordEncoder.encode(userCreationPayload.getPassword()));
        userEntity.setName(userCreationPayload.getName());
        if (userCreationPayload.getRole() == Role.WAITER) {
            userEntity.setRole(Role.WAITER);
            userEntity.setTables(new ArrayList<>());
        } else {
            userEntity.setRole(Role.MANAGER);
        }
        userRepository.save(userEntity);
    }

    @Override
    public List<UserPayload> getAllWaiters() {
        return userRepository.findAll().stream()
                .filter(userEntity -> userEntity.getRole() == Role.WAITER)
                .map(userMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void updateWaiter(TableAssignmentPayload tableAssignmentPayload) {
        UserEntity userEntity = getWaiterById(tableAssignmentPayload.getUserId());
        TableEntity tableEntity = tableService.getTableById(tableAssignmentPayload.getTableId());
        updateTables(tableEntity, userEntity);
        userRepository.save(userEntity);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("No user with username: " + username);
        }

        UserPrincipal userPrincipal = new UserPrincipal();
        userPrincipal.setId(user.getId());
        userPrincipal.setUsername(user.getUsername());
        userPrincipal.setPassword(user.getPassword());
        userPrincipal.setName(user.getName());
        userPrincipal.setRole(user.getRole());

        return userPrincipal;
    }

    @Override
    public UserEntity getWaiterById(Long waiterId) {
        UserEntity waiter = userRepository.findById(waiterId)
                .orElseThrow(() -> {
                    final String errorMsg = bundle.getString("cafe.manager.user.not.found");
                    log.error(errorMsg);
                    return new NotFoundException(errorMsg);
                });
        if (waiter.getRole() != Role.WAITER) {
            final String errorMsg = bundle.getString("cafe.manager.waiter.not.found");
            log.error(errorMsg);
            throw new NotFoundException(errorMsg);
        }
        return waiter;
    }

    /**
     * Updates the given userEntity's tables in this way:
     * if table is already assigned to that waiter, then table is unassigned,
     * in every other way assigns table to given entity
     *
     * @param tableEntity table, to which waiter should be assigned
     * @param userEntity  waiter, which is to assign
     * @return waiter entity with updated list of tables
     * @throws BadRequestException if the table is not empty
     */
    private void updateTables(TableEntity tableEntity, UserEntity userEntity) {
        if (tableEntity.getTableStatus() == TableStatus.USED) {
            final String errorMsg = bundle.getString("cafe.manager.table.not.empty");
            log.error(errorMsg);
            throw new BadRequestException(errorMsg);
        }
        if (tableEntity.getUserEntity() != null && tableEntity.getUserEntity().getId().equals(userEntity.getId())) {
            tableEntity.setUserEntity(null); // due to cascade type: merge setting user entity to null results in
            // that when user is merged, table record gets updated too
            // therefore waiter is not assigned to that table anymore
            tableEntity.setAssigned(false);
        } else {
            tableEntity.setUserEntity(userEntity);
            tableEntity.setAssigned(true);
            userEntity.getTables().add(tableEntity);
        }
    }
}
