package com.sflpro.assignment.cafe.manager.domain.dto.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginPayload {

    private String username;
    private String password;

}
