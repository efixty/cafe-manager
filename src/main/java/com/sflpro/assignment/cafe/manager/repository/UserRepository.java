package com.sflpro.assignment.cafe.manager.repository;

import com.sflpro.assignment.cafe.manager.domain.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {

    List<UserEntity> findAll();

    UserEntity findByUsername(String username);

}
