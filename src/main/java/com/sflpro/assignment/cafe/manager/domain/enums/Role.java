package com.sflpro.assignment.cafe.manager.domain.enums;

public enum Role {

    MANAGER("ROLE_MANAGER"),
    WAITER("ROLE_WAITER");

    private String role;

    Role(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
