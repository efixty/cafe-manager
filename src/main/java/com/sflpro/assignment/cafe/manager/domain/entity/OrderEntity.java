package com.sflpro.assignment.cafe.manager.domain.entity;

import com.sflpro.assignment.cafe.manager.domain.entity.base.BaseEntity;
import com.sflpro.assignment.cafe.manager.domain.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ORDERS")
public class OrderEntity extends BaseEntity {

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private OrderStatus orderStatus;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<ProductInOrderEntity> productsInOrder;

    @ManyToOne
    @JoinColumn(name = "TABLE_ID", nullable = false)
    private TableEntity tableEntity;

}
