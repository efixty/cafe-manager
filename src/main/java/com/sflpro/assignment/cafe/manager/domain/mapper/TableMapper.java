package com.sflpro.assignment.cafe.manager.domain.mapper;

import com.sflpro.assignment.cafe.manager.domain.dto.table.TablePayload;
import com.sflpro.assignment.cafe.manager.domain.entity.TableEntity;
import com.sflpro.assignment.cafe.manager.domain.mapper.base.Mapper;
import org.springframework.stereotype.Component;

@Component
public class TableMapper implements Mapper<TableEntity, TablePayload> {

    private final UserMapper userMapper;

    public TableMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public TablePayload map(TableEntity tableEntity) {
        TablePayload tablePayload = new TablePayload();
        tablePayload.setId(tableEntity.getId());
        tablePayload.setTableStatus(tableEntity.getTableStatus());
        tablePayload.setAssigned(tableEntity.getAssigned());
        if (tablePayload.getAssigned()) {
            tablePayload.setUserPayload(userMapper.map(tableEntity.getUserEntity()));
        } else {
            tablePayload.setUserPayload(null);
        }
        return tablePayload;
    }

    @Override
    public TableEntity map(TablePayload tablePayload) {
        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(tablePayload.getId());
        tableEntity.setUserEntity(userMapper.map(tablePayload.getUserPayload()));
        tableEntity.setTableStatus(tablePayload.getTableStatus());
        return tableEntity;
    }
}
