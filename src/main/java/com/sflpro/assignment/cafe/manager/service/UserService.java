package com.sflpro.assignment.cafe.manager.service;

import com.sflpro.assignment.cafe.manager.domain.dto.table.TableAssignmentPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserPayload;
import com.sflpro.assignment.cafe.manager.domain.entity.UserEntity;
import com.sflpro.assignment.cafe.manager.domain.exception.NotFoundException;

import java.util.List;

public interface UserService {

    /**
     * Creates a new user
     *
     * @param userCreationPayload payload containing all necessary information for user creation
     */
    void createUser(UserCreationPayload userCreationPayload);

    /**
     * Fetches all waiters from database
     *
     * @return list of waiter payloads
     */
    List<UserPayload> getAllWaiters();

    /**
     * Assigns (or unassign if already assigned) table with specified tableId to waiter with specified waiterId.
     *
     * @param tableAssignmentPayload dto, containing needed information for assignment
     */
    void updateWaiter(TableAssignmentPayload tableAssignmentPayload);

    /**
     * Utility method, intended for use within services
     * Fetches waiter by id
     *
     * @param waiterId id of requested waiter
     * @return user with id of waiterId and role of WAITER
     * @throws NotFoundException if user with such id not found or he is not a waiter
     */
    UserEntity getWaiterById(Long waiterId);
}
