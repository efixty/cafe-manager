package com.sflpro.assignment.cafe.manager.domain.dto.order;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderPayload;
import com.sflpro.assignment.cafe.manager.domain.enums.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderPayload {

    private Long id;
    private Long tableId;
    private Long userId;
    private List<ProductInOrderPayload> productsInOrder;
    private OrderStatus orderStatus;

}
