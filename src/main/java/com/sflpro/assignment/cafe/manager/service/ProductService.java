package com.sflpro.assignment.cafe.manager.service;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductPayload;

import java.util.List;

public interface ProductService {

    /**
     * Creates a new product
     *
     * @param productCreationPayload payload containing all necessary information for user creation
     */
    void createProduct(ProductCreationPayload productCreationPayload);

    /**
     * Fetches all products from local cache and maps them to corresponding payloads
     *
     * @return list of all products
     */
    List<ProductPayload> getMenu();
}
