package com.sflpro.assignment.cafe.manager.domain.mapper.base;

import com.sflpro.assignment.cafe.manager.domain.entity.base.BaseEntity;

public interface Mapper<T extends BaseEntity, S> {

    S map(T t);

    T map(S s);
}
