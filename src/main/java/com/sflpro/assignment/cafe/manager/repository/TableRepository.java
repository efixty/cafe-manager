package com.sflpro.assignment.cafe.manager.repository;

import com.sflpro.assignment.cafe.manager.domain.entity.TableEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TableRepository extends CrudRepository<TableEntity, Long> {

    List<TableEntity> findAll();

    /**
     * Finds all tables that are assigned to waiter with given id
     *
     * @param waiterId id of waiter whose tables are to be found
     * @return list of tables assigned to given waiter
     */
    @Query(nativeQuery = true, value = "select * from TABLES t where t.USER_ID = :waiterId")
    List<TableEntity> findAllByWaiterId(@Param("waiterId") Long waiterId);
}
