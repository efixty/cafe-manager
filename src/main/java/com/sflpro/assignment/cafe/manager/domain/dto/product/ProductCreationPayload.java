package com.sflpro.assignment.cafe.manager.domain.dto.product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductCreationPayload {

    private String name;
    private Double price;

}
