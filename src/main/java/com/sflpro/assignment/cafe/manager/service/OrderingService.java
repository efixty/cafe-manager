package com.sflpro.assignment.cafe.manager.service;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.order.OrderCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.order.OrderPayload;
import com.sflpro.assignment.cafe.manager.domain.entity.OrderEntity;
import com.sflpro.assignment.cafe.manager.domain.exception.NotFoundException;

import java.util.List;

public interface OrderingService {

    /**
     * Creates new order if the table with specified id is empty and has a waiter assigned.
     *
     * @param orderCreationPayload dto, containing all needed information for order creation
     * @return newly created order for creator to verify
     */
    OrderPayload createOrder(OrderCreationPayload orderCreationPayload);

    /**
     * Cancels the order (which means no payment will follow) if it is active
     *
     * @param tableId id of the table which order is to be canceled
     */
    void cancelOrder(Long tableId);

    /**
     * Closes the order if the order is still active
     *
     * @param tableId id of the table which order is to be closed
     * @return total amount to be payed
     */
    Double closeOrder(Long tableId);

    /**
     * Updates products in the order. If the product already exist in the order then it's status
     * and quantity are to be updated. Otherwise new product is simply added to the order.
     *
     * @param tableId            id of the table which order's product list should be updated
     * @param newProductsInOrder list of products for order to be updated with
     * @return new list of products in order for updater to verify
     */
    List<ProductInOrderPayload> updateProductsInOrder(Long tableId, List<ProductInOrderCreationPayload> newProductsInOrder);

    /**
     * Finds all active orders which must be served by currently logged in waiter
     *
     * @return list of order payloads which must be served by logged in waiter
     */
    List<OrderPayload> getActiveOrders();
}