package com.sflpro.assignment.cafe.manager.domain.mapper;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.order.OrderPayload;
import com.sflpro.assignment.cafe.manager.domain.entity.OrderEntity;
import com.sflpro.assignment.cafe.manager.domain.entity.ProductInOrderEntity;
import com.sflpro.assignment.cafe.manager.domain.mapper.base.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderMapper implements Mapper<OrderEntity, OrderPayload> {

    private final ProductInOrderMapper productInOrderMapper;

    public OrderMapper(ProductInOrderMapper productInOrderMapper) {
        this.productInOrderMapper = productInOrderMapper;
    }

    @Override
    public OrderPayload map(OrderEntity orderEntity) {
        OrderPayload orderPayload = new OrderPayload();
        orderPayload.setId(orderEntity.getId());
        List<ProductInOrderPayload> productsInOrder = orderEntity.getProductsInOrder()
                .stream()
                .map(productInOrderMapper::map)
                .collect(Collectors.toList());
        orderPayload.setProductsInOrder(productsInOrder);
        orderPayload.setTableId(orderEntity.getTableEntity().getId());
        orderPayload.setOrderStatus(orderEntity.getOrderStatus());
        orderPayload.setUserId(orderEntity.getTableEntity().getUserEntity().getId());
        return orderPayload;
    }

    @Override
    public OrderEntity map(OrderPayload orderPayload) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(orderPayload.getId());
        List<ProductInOrderEntity> productsInOrder = orderPayload.getProductsInOrder()
                .stream()
                .map(productInOrderMapper::map)
                .collect(Collectors.toList());
        orderEntity.setProductsInOrder(productsInOrder);
        orderEntity.setOrderStatus(orderPayload.getOrderStatus());
        return orderEntity;
    }
}
