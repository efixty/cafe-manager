package com.sflpro.assignment.cafe.manager.domain.dto.order;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderCreationPayload;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderCreationPayload {

    private Long tableId;
    private List<ProductInOrderCreationPayload> productInOrderCreationPayloads;

}
