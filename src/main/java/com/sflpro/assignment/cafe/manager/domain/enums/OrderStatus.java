package com.sflpro.assignment.cafe.manager.domain.enums;

public enum OrderStatus {
    ACTIVE,
    CLOSED,
    CANCELED
}
