package com.sflpro.assignment.cafe.manager.domain.mapper;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderPayload;
import com.sflpro.assignment.cafe.manager.domain.entity.ProductInOrderEntity;
import com.sflpro.assignment.cafe.manager.domain.mapper.base.Mapper;
import org.springframework.stereotype.Component;

@Component
public class ProductInOrderMapper implements Mapper<ProductInOrderEntity, ProductInOrderPayload> {

    private final ProductMapper productMapper;

    public ProductInOrderMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Override
    public ProductInOrderPayload map(ProductInOrderEntity productInOrderEntity) {
        ProductInOrderPayload productInOrderPayload = new ProductInOrderPayload();
        productInOrderPayload.setProductPayload(productMapper.map(productInOrderEntity.getProduct()));
        productInOrderPayload.setOrderedProductStatus(productInOrderEntity.getOrderedProductStatus());
        productInOrderPayload.setQuantity(productInOrderEntity.getQuantity());
        return productInOrderPayload;
    }

    @Override
    public ProductInOrderEntity map(ProductInOrderPayload productInOrderPayload) {
        ProductInOrderEntity productInOrderEntity = new ProductInOrderEntity();
        productInOrderEntity.setProduct(productMapper.map(productInOrderPayload.getProductPayload()));
        productInOrderEntity.setOrderedProductStatus(productInOrderPayload.getOrderedProductStatus());
        productInOrderEntity.setQuantity(productInOrderPayload.getQuantity());
        return productInOrderEntity;
    }
}
