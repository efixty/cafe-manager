package com.sflpro.assignment.cafe.manager.domain.dto.table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserPayload;
import com.sflpro.assignment.cafe.manager.domain.enums.TableStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TablePayload {

    private Long id;
    private UserPayload userPayload;
    private TableStatus tableStatus;
    private Boolean assigned;

}
