package com.sflpro.assignment.cafe.manager.rest;

import com.sflpro.assignment.cafe.manager.domain.dto.table.TableAssignmentPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.user.UserCreationPayload;
import com.sflpro.assignment.cafe.manager.service.OrderingService;
import com.sflpro.assignment.cafe.manager.service.UserService;
import com.sflpro.assignment.cafe.manager.service.impl.OrderingServiceImpl;
import com.sflpro.assignment.cafe.manager.service.impl.TableServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final OrderingService orderingService;
    private final TableServiceImpl tableService;
    private final UserService userService;

    public UserController(OrderingServiceImpl orderingService, TableServiceImpl tableService, UserService userService) {
        this.orderingService = orderingService;
        this.tableService = tableService;
        this.userService = userService;
    }

    /**
     * Creates a new user from given payload.
     *
     * @param userCreationPayload dto, containing all necessary information for user creation
     * @return
     */
    @PreAuthorize("hasRole('MANAGER')")
    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody UserCreationPayload userCreationPayload) {
        userService.createUser(userCreationPayload);
        return ResponseEntity.ok().build();
    }

    /**
     * Fetches all waiters from database.
     *
     * @return list of waiters
     */
    @PreAuthorize("hasRole('MANAGER')")
    @GetMapping("/waiters")
    public ResponseEntity<?> getWaiters() {
        return ResponseEntity.ok(userService.getAllWaiters());
    }

    /**
     * Returns all tables that are assigned to the currently logged in waiter.
     *
     * @return list of tables assigned to that waiter
     */
    @PreAuthorize("hasRole('WAITER')")
    @GetMapping("/waiter/tables")
    public ResponseEntity<?> getAssignedTables() {
        return ResponseEntity.ok(tableService.getTablePayloads());
    }

    /**
     * Fetches active orders that must be served by currently logged in waiter
     *
     * @return list of orders assigned to that waiter
     */
    @PreAuthorize("hasRole('WAITER')")
    @GetMapping("/waiter/orders")
    public ResponseEntity<?> getActiveOrders() {
        return ResponseEntity.ok(orderingService.getActiveOrders());
    }

    /**
     * Assigns (or unassignes if already assigned) the waiter with the specified id to the table with specified id.
     *
     * @param tableAssignmentPayload dto, containing the waiter and table ids
     * @return
     */
    @PreAuthorize("hasRole('MANAGER')")
    @PatchMapping("/assign")
    public ResponseEntity<?> assignToWaiter(@RequestBody TableAssignmentPayload tableAssignmentPayload) {
        userService.updateWaiter(tableAssignmentPayload);
        return ResponseEntity.ok().build();
    }
}