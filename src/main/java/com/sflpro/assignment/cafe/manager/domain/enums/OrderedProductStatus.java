package com.sflpro.assignment.cafe.manager.domain.enums;

public enum OrderedProductStatus {

    ACTIVE,
    CANCELED,
}
