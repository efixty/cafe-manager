package com.sflpro.assignment.cafe.manager.rest;

import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.product.ProductInOrderPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.order.OrderCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.order.OrderPayload;
import com.sflpro.assignment.cafe.manager.service.OrderingService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderingService orderingService;

    public OrderController(OrderingService orderingService) {
        this.orderingService = orderingService;
    }

    /**
     * Creates a new order if table for it is empty and assigned to a waiter, then returns it.
     *
     * @param orderCreationPayload dto, carrying all needed information about new order.
     * @return newly created order's dto
     */
    @PreAuthorize("hasRole('WAITER')")
    @PostMapping("/make")
    public ResponseEntity<?> createOrder(@RequestBody OrderCreationPayload orderCreationPayload) {
        OrderPayload eventualOrder = orderingService.createOrder(orderCreationPayload);
        return ResponseEntity.ok(eventualOrder);
    }

    /**
     * Cancels the order with the given id if it is active.
     *
     * @param tableId id of table which order is to cancel.
     * @return
     */
    @PreAuthorize("hasRole('WAITER')")
    @GetMapping("/cancel")
    public ResponseEntity<?> cancelOrder(@RequestParam("tableId") Long tableId) {
        orderingService.cancelOrder(tableId);
        return ResponseEntity.ok().build();
    }

    /**
     * Closes the order if its status is ACTIVE and return the total amount to pay.
     *
     * @param tableId id of table which order is to close.
     * @return the total amount to pay.
     */
    @PreAuthorize("hasRole('WAITER')")
    @GetMapping("/close")
    public ResponseEntity<?> closeOrder(@RequestParam("tableId") Long tableId) {
        return ResponseEntity.ok(orderingService.closeOrder(tableId));
    }

    /**
     * Updates products in the order with specified id and returns the new list of products in order.
     *
     * @param tableId                        id of the table which order is to update.
     * @param productInOrderCreationPayloads list of products to be updated
     * @return new products in order
     */
    @PreAuthorize("hasRole('WAITER')")
    @PutMapping("/update")
    public ResponseEntity<?> updateProductsInOrder(@RequestParam("tableId") Long tableId, @RequestBody List<ProductInOrderCreationPayload> productInOrderCreationPayloads) {
        List<ProductInOrderPayload> updatedProductsInOrder = orderingService.updateProductsInOrder(tableId, productInOrderCreationPayloads);
        return ResponseEntity.ok(updatedProductsInOrder);
    }
}
