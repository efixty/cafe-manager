package com.sflpro.assignment.cafe.manager.domain.enums;

public enum TableStatus {

    EMPTY,
    USED

}
