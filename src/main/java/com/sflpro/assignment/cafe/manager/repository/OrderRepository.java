package com.sflpro.assignment.cafe.manager.repository;

import com.sflpro.assignment.cafe.manager.domain.entity.OrderEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Long> {

    List<OrderEntity> findAll();

    /**
     * Finds all orders assigned to table which is assigned to waiter with given id
     *
     * @param waiterId id of waiter whose orders are to find
     * @return list of orders which are to be served by waiter with given id
     */
    @Query(nativeQuery = true, value = "select * from orders o where table_id in (select table_id from tables where user_id = :waiterId) and o.status = 0")
    List<OrderEntity> getActiveOrdersByWaiterId(@Param("waiterId") Long waiterId);

    /**
     * Finds active order which is assigned to table with given id
     *
     * @param tableId id of table which order is to find
     * @return active order entity which is assigned to table with given id
     */
    @Query(nativeQuery = true, value = "select * from orders o where table_id = :tableId and o.status = 0")
    OrderEntity getActiveOrderByTableId(@Param("tableId") Long tableId);
}
