package com.sflpro.assignment.cafe.manager.domain.mapper;

import com.sflpro.assignment.cafe.manager.domain.dto.user.UserPayload;
import com.sflpro.assignment.cafe.manager.domain.entity.UserEntity;
import com.sflpro.assignment.cafe.manager.domain.mapper.base.Mapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements Mapper<UserEntity, UserPayload> {

    @Override
    public UserPayload map(UserEntity userEntity) {
        UserPayload userPayload = new UserPayload();
        userPayload.setId(userEntity.getId());
        userPayload.setName(userEntity.getName());
        userPayload.setRole(userEntity.getRole());
        return userPayload;
    }

    @Override
    public UserEntity map(UserPayload userPayload) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userPayload.getId());
        userEntity.setName(userPayload.getName());
        userEntity.setRole(userPayload.getRole());
        return userEntity;
    }
}
