package com.sflpro.assignment.cafe.manager.domain.entity;

import com.sflpro.assignment.cafe.manager.domain.entity.base.BaseEntity;
import com.sflpro.assignment.cafe.manager.domain.enums.TableStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TABLES")
public class TableEntity extends BaseEntity {

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private TableStatus tableStatus;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.MERGE,
                    CascadeType.PERSIST,
                    CascadeType.REFRESH
            },
            mappedBy = "tableEntity")
    private List<OrderEntity> orderEntityList;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity userEntity;

    @Column(name = "SEAT_COUNT", nullable = false)
    private Byte seatCount;

    @Column(name = "ASSIGNED", nullable = false)
    private Boolean assigned;
}
