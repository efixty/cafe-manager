package com.sflpro.assignment.cafe.manager.rest;

import com.sflpro.assignment.cafe.manager.domain.dto.table.TableCreationPayload;
import com.sflpro.assignment.cafe.manager.service.TableService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tables")
public class TableController {

    private final TableService tableService;

    public TableController(TableService tableService) {
        this.tableService = tableService;
    }

    /**
     * Creates a new table from given payload.
     *
     * @param tableCreationPayload dto, containing all necessary information for table creation
     * @return
     */
    @PreAuthorize("hasRole('MANAGER')")
    @PostMapping("/create")
    public ResponseEntity<?> createTable(@RequestBody TableCreationPayload tableCreationPayload) {
        tableService.createTable(tableCreationPayload);
        return ResponseEntity.ok().build();
    }

    /**
     * Fetches all tables from the database.
     *
     * @return list of tables
     */
    @PreAuthorize("hasRole('MANAGER')")
    @GetMapping
    public ResponseEntity<?> getTables() {
        return ResponseEntity.ok(tableService.getAllTables());
    }
}
