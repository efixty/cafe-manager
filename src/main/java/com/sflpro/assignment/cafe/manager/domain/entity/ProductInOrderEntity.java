package com.sflpro.assignment.cafe.manager.domain.entity;

import com.sflpro.assignment.cafe.manager.domain.entity.base.BaseEntity;
import com.sflpro.assignment.cafe.manager.domain.enums.OrderedProductStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PRODUCT_IN_ORDER")
public class ProductInOrderEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", nullable = false)
    private ProductEntity product;

    @ManyToOne
    @JoinColumn(name = "ORDER_ID", nullable = false)
    private OrderEntity order;

    @Column(name = "QUANTITY", nullable = false)
    private Integer quantity;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private OrderedProductStatus orderedProductStatus;

}
