package com.sflpro.assignment.cafe.manager.config.security.util;

import com.sflpro.assignment.cafe.manager.domain.dto.user.UserPrincipal;
import com.sflpro.assignment.cafe.manager.domain.enums.Role;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class JwtTokenUtils {

    private static final long EXPIRATION_TIME = 10 * 3600 * 1000; // 10 hours

    private static final String SECRET = "MostSecureSecretInThisInsecureWorld";

    private static final String AUTHORITIES = "Authorities";

    private static final String ID = "id";

    private static final String SUBJECT = "sub";

    private static final String TOKEN_PREFIX = "Bearer";

    private static final String NAME = "name";

    private static final String PASSWORD = "password";

    private static final String ROLE = "role";

    public static String generateAuthHeader(Authentication auth) throws UnsupportedEncodingException {

        final List<String> authorities = auth.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        final Map<String, Object> claims = new HashMap<>();

        UserPrincipal principal = (UserPrincipal) auth.getPrincipal();
        claims.put(ID, principal.getId().toString());
        claims.put(SUBJECT, principal.getUsername());
        claims.put(NAME, principal.getName());
        claims.put(AUTHORITIES, authorities);
        claims.put(PASSWORD, principal.getPassword());
        claims.put(ROLE, principal.getRole());

        String jwt = Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();

        return URLEncoder.encode(TOKEN_PREFIX + " " + jwt, "UTF-8");
    }

    public static Authentication parseAuthHeader(String authHeader) throws UnsupportedEncodingException {
        authHeader = URLDecoder.decode(authHeader, "UTF-8");

        String authToken = authHeader.replace(TOKEN_PREFIX + " ", "");

        Claims claims = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(authToken)
                .getBody();

        String username = claims.getSubject();

        @SuppressWarnings("unchecked") final List<String> authoritiesClaim = (List<String>) claims.get(AUTHORITIES);

        final List<SimpleGrantedAuthority> authorities = authoritiesClaim
                .stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        Long id = Long.valueOf(claims.get(ID, String.class));
        Role role = Role.valueOf(claims.get(ROLE, String.class));
        String name = claims.get(NAME, String.class);
        String password = claims.get(PASSWORD, String.class);

        UserPrincipal userPrincipal = new UserPrincipal();
        userPrincipal.setId(id);
        userPrincipal.setUsername(username);
        userPrincipal.setRole(role);
        userPrincipal.setName(name);
        userPrincipal.setPassword(password);

        return username != null ?
                new UsernamePasswordAuthenticationToken(userPrincipal, null, authorities) :
                null;
    }

}