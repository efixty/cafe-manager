package com.sflpro.assignment.cafe.manager.service;

import com.sflpro.assignment.cafe.manager.domain.dto.table.TableCreationPayload;
import com.sflpro.assignment.cafe.manager.domain.dto.table.TablePayload;
import com.sflpro.assignment.cafe.manager.domain.entity.TableEntity;
import com.sflpro.assignment.cafe.manager.domain.exception.BadRequestException;
import com.sflpro.assignment.cafe.manager.domain.exception.NotFoundException;

import java.util.List;

public interface TableService {

    /**
     * Creates a new table
     *
     * @param tableCreationPayload payload containing all necessary information for user creation
     */
    void createTable(TableCreationPayload tableCreationPayload);

    /**
     * Fetches all tables from database
     *
     * @return list of table payloads
     */
    List<TablePayload> getAllTables();

    /**
     * Fetches all tables assigned to currently logged in waiter and maps them to corresponding payloads
     *
     * @return list of table payloads
     */
    List<TablePayload> getTablePayloads();

    /**
     * Checks if requested table is eligible to be used
     *
     * @param tableEntity table to be checked
     * @throws BadRequestException if table has no waiters assigned or if table is already in use
     */
    void validateTableAvailability(TableEntity tableEntity);

    /**
     * Utility method, intended for use within services
     * Fetches table by id
     *
     * @param tableId id of the order
     * @return entity of the table with specified id
     * @throws NotFoundException if table with such id not found
     */
    TableEntity getTableById(Long tableId);
}