package com.sflpro.assignment.cafe.manager.config.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sflpro.assignment.cafe.manager.config.security.util.JwtTokenUtils;
import com.sflpro.assignment.cafe.manager.domain.dto.user.LoginPayload;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    public JWTLoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        this.setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse resp)
            throws AuthenticationException, IOException {

        LoginPayload loginPayload = new ObjectMapper().readValue(req.getInputStream(), LoginPayload.class);

        return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(loginPayload.getUsername(),
                loginPayload.getPassword())
        );
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse resp,
                                            FilterChain chain, Authentication auth) throws UnsupportedEncodingException {
        String token = JwtTokenUtils.generateAuthHeader(auth);
        Cookie cookie = new Cookie("Authorization", token);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setSecure(true);
        resp.addCookie(cookie);
    }
}
