package com.sflpro.assignment.cafe.manager.domain.dto.product;

import com.sflpro.assignment.cafe.manager.domain.enums.OrderedProductStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductInOrderPayload {

    private ProductPayload productPayload;
    private int quantity;
    private OrderedProductStatus orderedProductStatus;

}
