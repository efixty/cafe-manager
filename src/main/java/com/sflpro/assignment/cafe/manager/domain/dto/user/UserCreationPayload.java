package com.sflpro.assignment.cafe.manager.domain.dto.user;

import com.sflpro.assignment.cafe.manager.domain.enums.Role;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCreationPayload {

    private String username;
    private String password;
    private String name;
    private Role role;

}
